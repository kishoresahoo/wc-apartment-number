jQuery('input#billing_apartment_number').live('change', function(){
			
			var billing_apartment_number = jQuery('input#billing_apartment_number').val();
			
			if (billing_apartment_number && jQuery.isNumeric( billing_apartment_number ) && billing_apartment_number >= 0) {
				jQuery('#billing_apartment_number_field').addClass('woocommerce-validated');
				jQuery('#billing_apartment_number_field').removeClass('woocommerce-invalid');

			} else if (billing_apartment_number && !jQuery.isNumeric( billing_apartment_number ) ) {
    			jQuery('#billing_apartment_number_field').removeClass('woocommerce-validated');
				jQuery('#billing_apartment_number_field').addClass('woocommerce-invalid');
			} else {
				jQuery('#billing_apartment_number_field').removeClass('woocommerce-validated');
				jQuery('#billing_apartment_number_field').removeClass('woocommerce-invalid');
			}
			
});

jQuery('input#shipping_apartment_number').live('change', function(){
			
			var shipping_apartment_number = jQuery('input#shipping_apartment_number').val();
			
			if (shipping_apartment_number && jQuery.isNumeric( shipping_apartment_number ) && shipping_apartment_number >= 0) {
				jQuery('#shipping_apartment_number_field').addClass('woocommerce-validated');
				jQuery('#shipping_apartment_number_field').removeClass('woocommerce-invalid');

			} else if (shipping_apartment_number && !jQuery.isNumeric( shipping_apartment_number ) ) {
    			jQuery('#shipping_apartment_number_field').removeClass('woocommerce-validated');
				jQuery('#shipping_apartment_number_field').addClass('woocommerce-invalid');
			} else {
				jQuery('#shipping_apartment_number_field').removeClass('woocommerce-validated');
				jQuery('#shipping_apartment_number_field').removeClass('woocommerce-invalid');
			}
			
});