<?php
/**
 * Main class.
 *
 * @package WC_Apartment_Number
 */

if ( ! class_exists( 'WC_Apartment_Number' ) ) {

	/**
	 * Handles core plugin hooks and action setup.
	 */
	class WC_Apartment_Number {

		/**
		 * Returns the instance.
		 */
		public static function get_instance() {

			static $instance = null;

			if ( is_null( $instance ) ) {
				$instance = new self();
				$instance->setup_actions();
			}

			return $instance;
		}

		/**
		 * Constructor method.
		 */
		public function __construct() {
			if ( defined( 'WC_APARTMENT_NUMBER_VERSION' ) ) {
				$this->version = WC_APARTMENT_NUMBER_VERSION;
			} else {
				$this->version = '1.0.0';
			}

			$this->plugin_name = 'wc-apartment-number';
		}

		/**
		 * Sets up initial actions.
		 *
		 * @since  1.0.0
		 * @access private
		 * @return void
		 */
		private function setup_actions() {
			// Register scripts, styles, and fonts.
			add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );

			add_filter( 'woocommerce_default_address_fields', array( $this, 'customise_default_address_fields' ) );
			add_filter( 'woocommerce_checkout_process', array( $this, 'customise_checkout_field_process' ) );
			add_filter( 'woocommerce_get_order_address', array( $this, 'order_address' ), 10, 3);
			add_filter( 'woocommerce_admin_billing_fields', array( $this, 'admin_billing_fields' ) );
			add_filter( 'woocommerce_admin_shipping_fields', array( $this, 'admin_shipping_fields' ) );

			add_filter( 'woocommerce_formatted_address_replacements', array( $this, 
				'custom_woocommerce_formatted_address_replacements' ), 10, 2);
			add_filter( 'woocommerce_localisation_address_formats', array( $this, 
				'custom_woocommerce_localisation_address_formats' ) );
		}

		/**
		 * Customise default address fields
		 *
		 * Returns the fields we show by default. This can be filtered later on.
		 * @param  array $fields Default address fields. 
		 * @return array
		 */
		public function customise_default_address_fields( $fields ){

			$apartment_number_field = array(
					'apartment_number' => array(
						'label'        => __( 'Apartment Number', 'woocommerce' ),
						'required'     => false,
						'class'        => array( 'form-row-wide', 'address-field' ),
						'validate'     => array( 'number' ),
						'priority'     => 55,
					)
			);

			$fields = array_merge($fields, $apartment_number_field);
		    
		    return $fields;
		}

		
		/**
		 * Process the checkout after the confirm order button is pressed.
		 */
		public function customise_checkout_field_process() {
			// if the field is set, if not then show an error message.
			if ( ! empty( $_POST['billing_apartment_number'] ) && ! is_numeric( $_POST['billing_apartment_number'] ) ) {
				wc_add_notice( sprintf( __( '%s is not a number.', 'woocommerce' ), '<strong>' . $_POST['billing_apartment_number'] . '</strong>' ), 'error' );
			}

			if (isset($_POST['ship_to_different_address'])) {
				if ( ! empty( $_POST['shipping_apartment_number'] ) && ! is_numeric($_POST['shipping_apartment_number']) ) {
					wc_add_notice( sprintf( __( '%s is not a number.', 'woocommerce' ), '<strong>' . $_POST['shipping_apartment_number'] . '</strong>' ), 'error' );
				}
			}
			
		}


		/**
		 * Order address.
		 *
		 * @param  array    $address Address data.
		 * @param  string   $type    Address type.
		 * @param  WC_Order $order   Order object.
		 * @return array
		 */
		public function order_address( $address, $type, $order ) {
		 	$apartment_number       = $type . '_apartment_number';
		 	$address['apartment_number']       = $order->get_meta( '_' . $apartment_number );
			return $address;
		}

		/** 
	   	 * Custom shop order billing fields. 
	   	 * 
	   	 * @param  array $fields Default order billing fields. 
	   	 * 
	   	 * @return array Custom order billing fields. 
	   	 */ 
		public function admin_billing_fields( $fields ) {
			global $post;
			$order_id = $post->ID;
			$value=get_post_meta( $order_id , '_billing_apartment_number', true);

			$insert = array('apartment_number' =>
				array(
				'label' => __('Apartment Number', 'woocommerce'),
				'show' => false ,
				'value' => $value,
			));

			$fields1 = array_slice($fields, 0, 4);
			$fields2 = array_slice($fields, 4);

			$fields = array_merge($fields1,$insert,$fields2);

			return $fields;
		}


		/** 
         * Custom shop order shipping fields. 
         * 
         * @param  array $fields Default order shipping fields. 
         * 
         * @return array  Custom order shipping fields. 
         */ 
		public function admin_shipping_fields( $fields ) {
			global $post;
			$order_id = $post->ID;
			$value=get_post_meta( $order_id , '_shipping_apartment_number', true);
			$insert = array('apartment_number' =>
				array(
				'label' => __('Apartment Number', 'woocommerce'),
				'show' => false ,
				'value' => $value,
			));

			$fields1 = array_slice($fields, 0, 4);
			$fields2 = array_slice($fields, 4);

			$fields = array_merge($fields1,$insert,$fields2);

			return $fields;
		}


		/**
		 * Custom country address format.
		 *
		 * @param  array $replacements Default replacements.
		 * @param  array $args         Arguments to replace.
		 *
		 * @return array               New replacements.
		 */
		function custom_woocommerce_formatted_address_replacements( $replacements, $args ) {
			$replacements['{apartment_number}']       = $args['apartment_number'];
			return $replacements;
		}


		/**
		 * Custom country address formats.
		 *
		 * @param  array $formats Defaul formats.
		 *
		 * @return array          New format.
		 */
		function custom_woocommerce_localisation_address_formats($formats) {
		    $formats[ 'default' ]  = "{company}\n{name}\n{address_1}\n{apartment_number}\n{address_2}\n{city} - {postcode}\n{state}, {country}";
			$formats['AU']="{name}\n{company}\n{address_1}\n{address_2}\n{city} {state} {postcode}\n{country}";
			$formats['AT']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['BE']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['CA']="{company}\n{name}\n{address_1}\n{address_2}\n{city} {state} {postcode}\n{country}";
			$formats['CH']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['CL']="{company}\n{name}\n{address_1}\n{address_2}\n{state}\n{postcode} {city}\n{country}";
			$formats['CN']="{country} {postcode}\n{state}, {city}, {address_2}, {address_1}\n{company}\n{name}";
			$formats['CZ']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['DE']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['EE']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['FI']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['DK']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['FR']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city_upper}\n{country}";
			$formats['HK']="{company}\n{first_name} {last_name_upper}\n{address_1}\n{address_2}\n{city_upper}\n{state_upper}\n{country}";
			$formats['HU']="{name}\n{company}\n{city}\n{address_1}\n{address_2}\n{postcode}\n{country}";
			$formats['IN']="{company}\n{name}\n{address_1}\n{apartment_number}\n{address_2}\n{city} - {postcode}\n{state}, {country}";
			$formats['IS']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['IT']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode}\n{city}\n{state_upper}\n{country}";
			$formats['JP']="{postcode}\n{state} {city} {address_1}\n{address_2}\n{company}\n{last_name} {first_name}\n{country}";
			$formats['TW']="{company}\n{last_name} {first_name}\n{address_1}\n{address_2}\n{state}, {city} {postcode}\n{country}";
			$formats['LI']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['NL']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['NZ']="{name}\n{company}\n{address_1}\n{address_2}\n{city} {postcode}\n{country}";
			$formats['NO']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['PL']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['PT']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['SK']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['SI']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['ES']="{name}\n{company}\n{address_1}\n{address_2}\n{postcode} {city}\n{state}\n{country}";
			$formats['SE']="{company}\n{name}\n{address_1}\n{address_2}\n{postcode} {city}\n{country}";
			$formats['TR']="{name}\n{company}\n{address_1}\n{address_2}\n{postcode} {city} {state}\n{country}";
			$formats['US']="{name}\n{company}\n{address_1}\n{address_2}\n{city}, {state_code} {postcode}\n{country}";
			$formats['VN']="{name}\n{company}\n{address_1}\n{city}\n{country}";

			return $formats;
		}

		/**
		 * Enqueue scripts.
		 */
		public function scripts() {
			wp_enqueue_script( $this->plugin_name . '-main', WC_APARTMENT_NUMBER_PLUGIN_URL . '/assets/js/wc-apartment-number.min.js', array( 'jquery' ), '1234567', false );
		}

	}
}
