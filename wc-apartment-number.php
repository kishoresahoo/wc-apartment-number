<?php
/**
 * Plugin Name: WooCommerce - Extra field for apartment number.
 * Plugin URI: https://bitbucket.org/kishoresahoo/wc-apartment-number/
 * Description: WooCommerce Extra field for apartment number
 * Version: 1.0.0
 * Author: Kishore Sahoo
 * Author URI: https://upnrunn.com/
 * Text Domain: woocommerce
 * License: GPL 3.0
 *
 * @package WC_Apartment_Number
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'WC_APARTMENT_NUMBER_VERSION', '1.0.0' );
define( 'WC_APARTMENT_NUMBER_PLUGIN_DIR', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'WC_APARTMENT_NUMBER_PLUGIN_URL', untrailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ) );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wc-apartment-number.php';

/**
 * Gets the instance of the `WC_Apartment_Number` class.  This function is useful for quickly grabbing data
 * used throughout the plugin.
 */
function wc_apartment_number() {
	return WC_Apartment_Number::get_instance();
}

// Let's roll!
wc_apartment_number();